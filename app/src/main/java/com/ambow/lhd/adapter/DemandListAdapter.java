package com.ambow.lhd.adapter;

import java.util.List;

import com.ambow.lhd.app.R;
import com.ambow.lhd.entity.ItemList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * 信息适配器
 * 商品
 * 求购
 * @author 林东
 *
 */
public class DemandListAdapter extends BaseAdapter {

	private List<ItemList> itemList;
	private LayoutInflater inflater;
	
	public List<ItemList> getItemList() {
		return itemList;
	}
	
	public void setItemList(List<ItemList> itemList) {
		this.itemList = itemList;
	}

	public DemandListAdapter(Context context){
		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return itemList == null ? 0 :itemList.size();
	}

	@Override
	public Object getItem(int position) {
		return itemList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		System.out.println("-----------到DemandListAdapter----------");
		ViewHolder vh = null;
		if(convertView == null){
			convertView = inflater.inflate(R.layout.information_list_item, null);
			vh = new ViewHolder();
			vh.list_item_tv_name = (TextView) convertView.findViewById(R.id.information_list_item_tv_name);
			vh.list_item_tv_school = (TextView) convertView.findViewById(R.id.information_list_item_tv_school);
			vh.list_item_tv_floor = (TextView) convertView.findViewById(R.id.information_list_item_tv_floor);
//			vh.list_item_tv_Price = (TextView) convertView.findViewById(R.id.information_list_item_tv_Price);
			convertView.setTag(vh);
		}else {
			vh = (ViewHolder) convertView.getTag();
		}
		ItemList il = itemList.get(position);
		vh.list_item_tv_name.setText(il.getShopName()+" ("+il.getCategory()+")");
		vh.list_item_tv_school.setText(il.getSchool());
		vh.list_item_tv_floor.setText(il.getCourt());
//		vh.list_item_tv_Price.setText(il.getPrice());
		return convertView;
	}
	
	public class ViewHolder{
		TextView list_item_tv_name;
		TextView list_item_tv_school;
		TextView list_item_tv_floor;
//		TextView list_item_tv_Price;
	}
}