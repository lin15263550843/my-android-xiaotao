package com.ambow.lhd.adapter;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import com.ambow.lhd.app.LeaveMessageActivity;
import com.ambow.lhd.app.R;
import com.ambow.lhd.entity.Messages;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * 信息适配器
 * 留言信息
 * 
 * @author 林东
 *
 */
public class MessageListAdapter extends BaseAdapter {

	private List<Messages> messagesList;
	private Context context;
	private LayoutInflater inflater;
	
	public List<Messages> getMessagesList() {
		return messagesList;
	}
 
	public void setMessagesList(List<Messages> messagesList) {
		this.messagesList = messagesList;
	}

	public MessageListAdapter(Context context){
		this.context = context;
		inflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return messagesList == null ? 0 :messagesList.size();
	}

	@Override
	public Object getItem(int position) {
		return messagesList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		System.out.println("-----------????到ShopListAdapter?????----------");
		ViewHolder vh = null;
		if(convertView == null){
			convertView = inflater.inflate(R.layout.message_list_item, null);
			vh = new ViewHolder();
			vh.list_item_tv_name = (TextView) convertView.findViewById(R.id.message_list_item_tv_name);
			vh.list_item_tv_content = (TextView) convertView.findViewById(R.id.message_list_item_tv_content);
			vh.list_item_tv_time = (TextView) convertView.findViewById(R.id.message_list_item_tv_time);
			vh.list_item_tv_reply = (TextView) convertView.findViewById(R.id.message_list_item_tv_reply);
			convertView.setTag(vh);
		}else {
			vh = (ViewHolder) convertView.getTag();
		}
		final Messages msg = messagesList.get(position);
		System.out.println("--------留言---------"+msg);
		vh.list_item_tv_name.setText(msg.getUsername());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		vh.list_item_tv_time.setText(sdf.format(msg.getLeave_time()));
		vh.list_item_tv_content.setText("　　"+msg.getContent());
		vh.list_item_tv_reply.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(context, LeaveMessageActivity.class);
				intent.putExtra("username", msg.getUsername());
				context.startActivity(intent);
			}
		});
		return convertView;
	}
	
	public class ViewHolder{
		TextView list_item_tv_name; //留言者姓名
		TextView list_item_tv_content; //留言内容
		TextView list_item_tv_time; //留言日期
		TextView list_item_tv_reply; //回复
	}
}