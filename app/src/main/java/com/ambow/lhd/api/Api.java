package com.ambow.lhd.api;
/**
 * 接口
 * @author 林东
 *
 */
public class Api {
	/** 与服务器端连接的协议名 */
	public static final String PROTOCOL = "http://";
	/** 服务器IP */
//	public static final String HOST = "10.0.2.2";  //AVD test
	public static final String HOST = "192.168.0.105"; 
	/** 服务器端口号 */
	public static final String PORT = "8080";
	public static final String URL = PROTOCOL+HOST+":"+PORT; 
}
