package com.ambow.lhd.app;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpStatus;

import com.ambow.lhd.api.Api;
import com.ambow.lhd.common.AppException;
import com.ambow.lhd.common.HttpHelper;
import com.ambow.lhd.common.HttpHelper.Callback;
import com.ambow.lhd.entity.ItemList;
import com.ambow.lhd.entity.Users;
import com.ambow.lhd.service.MyApplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 求购信息详情
 * 
 * @author 林东
 *
 */
public class DemandInfoActivity extends Activity implements OnClickListener {
	/**
	private TextView title, createTime, tx_title, price, type, location,
			describe, linkMan, link, tx_toPhone, tx_toMsg, tx_toLeaveMessage;
	private List<Bitmap> listBitmap = new ArrayList<Bitmap>();// 下载的图片集合
	private LinearLayout layout;
	private ViewPager vp;
	private PageIndicator indicator;
	private ArrayList<View> pageViews;
	private View view1, view2, view3;// 图片的布局
	private ImageView image1, image2, image3;// 图片显示
	*/
	private MyApplication myApplication;
	private Intent intent;
	private ItemList itemList; //物品信息详细信息集合
	private String userName; //用户名
	private ImageButton ib_back; // 返回
	private TextView title_tv_type; // 显示物品详情
	private TextView tv_update; //收藏
	private View dialogView; //对话框
	private Dialog dialog; //弹出对话框
	private Button btn_back; //取消登陆按钮
	private Button btn_determine; //确定登陆按钮 

	private ImageButton iv_pic; // 物品图片
	private TextView tv_sname; // 物品名称
	private TextView tv_type; // 物品类型
	private TextView tv_price; // 物品价格
	private TextView tv_phone; // 电话
	private TextView tv_school; // 所在学校
	private TextView tv_floor; // 所在宿舍楼
	private TextView tv_uname; // 发布人姓名
	private TextView tv_releaseTime; // 发布日期
	private TextView tv_description; // 物品描述
	
	private TextView tv_tel; // 打电话
	private TextView tv_smsto; //发信息
	private TextView tv_message; //留言
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.shop_information);

		myApplication = (MyApplication) this.getApplicationContext();
		myApplication.addActivity(this);
		Intent intent = getIntent();
		itemList = (ItemList) intent.getSerializableExtra("shopinfo");
		
		ib_back = (ImageButton) this.findViewById(R.id.type_title_ib_back);
		title_tv_type = (TextView) findViewById(R.id.type_title_tv_type);
		tv_update = (TextView) findViewById(R.id.type_title_tv_update);
		//确认对话框
		dialogView = getLayoutInflater().inflate(R.layout.alert_dialog, null, false);
		//创建对话框
		dialog = new Dialog(this, R.style.myDialogTheme);
		//对话框事件    
		//注意  必须通过<dialogView.findViewById()>查找布局文件中的指定Id的组件,findViewById()找不到组件
		btn_back = (Button) dialogView.findViewById(R.id.alert_dialog_btn_back);
		btn_determine = (Button) dialogView.findViewById(R.id.alert_dialog_btn_determine);
		
		tv_sname = (TextView) findViewById(R.id.shop_information_tv_sname);
		tv_type = (TextView) findViewById(R.id.shop_information_tv_type);
		tv_price = (TextView) findViewById(R.id.shop_information_tv_price);
		tv_phone = (TextView) findViewById(R.id.shop_information_tv_phone);
		tv_school = (TextView) findViewById(R.id.shop_information_tv_school);
		tv_floor = (TextView) findViewById(R.id.shop_information_tv_floor);
		tv_uname = (TextView) findViewById(R.id.shop_information_tv_uname);
		tv_releaseTime = (TextView) findViewById(R.id.shop_information_tv_releaseTime);
		tv_description = (TextView) findViewById(R.id.shop_information_tv_description);
		//底部栏
		tv_tel = (TextView) findViewById(R.id.shop_information_tv_tel);
		tv_smsto = (TextView) findViewById(R.id.shop_information_tv_smsto);
		tv_message = (TextView) findViewById(R.id.shop_information_tv_message);
		
		//注册监听事件
		ib_back.setOnClickListener(this);
		title_tv_type.setText("求购信息详情");
		tv_update.setOnClickListener(this);
		//更改TextView中文字左边的图片
		Drawable nav_up = getResources().getDrawable(R.drawable.collect);  
		nav_up.setBounds(0, 0, nav_up.getMinimumWidth(), nav_up.getMinimumHeight());  
		tv_update.setCompoundDrawables(nav_up, null, null, null); //left,top,right,bottom
		tv_update.setText("收藏"); //更改TextView总的文字 
		
		btn_back.setOnClickListener(this);
		btn_determine.setOnClickListener(this);
		//获取物品详细信息
		tv_sname.setText(itemList.getShopName());
		tv_type.setText(itemList.getCategory());
//		tv_price.setText(itemList.getPrice());
		tv_price.setText("求购");
		tv_phone.setText(itemList.getUserPhone());
		tv_school.setText(itemList.getSchool());
		tv_floor.setText(itemList.getCourt());
		tv_uname.setText(itemList.getUserName());
		Date date = itemList.getPutTime();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		tv_releaseTime.setText(sdf.format(date));
		tv_description.setText("　　"+itemList.getDescription());
		// 拨打电话
		tv_tel.setOnClickListener(this);
		//发送短信
		tv_smsto.setOnClickListener(this);
		//留言
		tv_message.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// 返回
		if (v == ib_back) {
			Intent intent = new Intent(this, MainActivity.class);
			this.startActivity(intent);
			this.finish();
		}
		//收藏
		if(v == tv_update){
			Users users = (Users) myApplication.userMap.get("user");
			if(users != null){
				userName = users.getUserName();
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("shopId", itemList.getShopId());
				params.put("userName", userName);
				HttpHelper.asyncPost(Api.URL + "/second-hand/collection_add.do", params, new Callback() {
					
					@Override
					public void dataLoaded(Message msg) {
						if (HttpStatus.SC_OK != msg.what) {
							AppException.http(msg.what).makeToast(DemandInfoActivity.this);
							return;
						}
						Toast.makeText(DemandInfoActivity.this, msg.obj.toString(), 0).show();
					}
				});
			} else {
				//弹出确认登陆对话框
				dialog.setContentView(dialogView);
				dialog.show();
			}
		}
		//对话框的取消按钮
		if(v == btn_back){
			//关闭对话框
			dialog.dismiss();
		}
		//对话框的确认按钮
		if(v == btn_determine){
			//确定后跳到登录页面          
			dialog.dismiss();
			intent = new Intent(this,LoginActivity.class);
			this.startActivity(intent);
		}
		//拨打电话
		if(v == tv_tel){
			
			intent = new Intent();
			//系统默认的action, 用来打开默认的电话界面
			intent.setAction(Intent.ACTION_DIAL);
			//需要拨打的号码
			intent.setData(Uri.parse("tel:" + tv_phone.getText().toString()));
			DemandInfoActivity.this.startActivity(intent);
		}
		//发送短信
		if(v == tv_smsto){
			
			intent = new Intent();
			// 系统默认的action，用来打开默认的短信界面
			intent.setAction(Intent.ACTION_SENDTO);
			// 需要发短息的号码
			intent.setData(Uri.parse("smsto:" + tv_phone.getText().toString()));
			DemandInfoActivity.this.startActivity(intent);
		}
		//留言
		if(v == tv_message){
			System.out.println("-------------跳转留言------------");
			intent = new Intent(this, LeaveMessageActivity.class);
			intent.putExtra("username", itemList.getUserName());
			this.startActivity(intent);
		}
	}
}
