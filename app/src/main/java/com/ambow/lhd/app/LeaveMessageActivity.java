package com.ambow.lhd.app;
/**
 * 留言板
 */
import java.util.HashMap;

import org.apache.http.HttpStatus;

import com.ambow.lhd.api.Api;
import com.ambow.lhd.common.AppException;
import com.ambow.lhd.common.HttpHelper;
import com.ambow.lhd.common.HttpHelper.Callback;
import com.ambow.lhd.entity.Users;
import com.ambow.lhd.service.MyApplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class LeaveMessageActivity extends Activity implements OnClickListener {

	private MyApplication myApplication;
	private Intent intent;
	private Users users;
	private String receiveName; //接收留言者
	private String userName; //发送留言这
	private String content; //留言内容
	
	private View dialogView; //对话框的视图
	private Dialog dialog; //弹出对话框
	private Button btn_back; //取消登陆按钮
	private Button btn_determine; //确定登陆按钮 
	
	private ImageButton ib_back; //返回上一页
	private TextView tv_title; //留言板 
	private TextView tv_update; //回首页
	
	private EditText et_content; //留言内容
	private Button btn_ok; //确认留言
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.leave_message);
		
		myApplication = (MyApplication) getApplicationContext();
		myApplication.addActivity(this);
		
		ib_back = (ImageButton) findViewById(R.id.type_title_ib_back);
		tv_title = (TextView) findViewById(R.id.type_title_tv_type);
		tv_update = (TextView) findViewById(R.id.type_title_tv_update);
		et_content = (EditText) findViewById(R.id.leave_message_et_content);
		btn_ok = (Button) findViewById(R.id.leave_message_btn_ok);
		//确认对话框
		dialogView = getLayoutInflater().inflate(R.layout.alert_dialog, null, false);
		//创建对话框
		dialog = new Dialog(this, R.style.myDialogTheme);
		//对话框事件    
		//注意  必须通过<dialogView.findViewById()>查找布局文件中的指定Id的组件,findViewById()找不到组件
		btn_back = (Button) dialogView.findViewById(R.id.alert_dialog_btn_back);
		btn_determine = (Button) dialogView.findViewById(R.id.alert_dialog_btn_determine);
		//注册监听事件
		ib_back.setOnClickListener(this);
		tv_title.setText("留言板");
		tv_update.setOnClickListener(this);
		tv_update.setCompoundDrawables(null, null, null, null); //不显示TextView中的图片
		tv_update.setText("返回首页");
		et_content.setText("　　");
		btn_ok.setOnClickListener(this);
		//对话框的监听事件
		btn_back.setOnClickListener(this); 
		btn_determine.setOnClickListener(this);
		ifLogin();  //判断是否登陆
		
	}
	//判断是否登陆
	private void ifLogin() {
		intent = getIntent();
		receiveName = intent.getStringExtra("username");
		users = (Users) myApplication.userMap.get("user");
		if (users != null) {
			userName = users.getUserName(); //获得留言者姓名
		} else {
			//弹出确认登陆对话框
			dialog.setContentView(dialogView);
			dialog.show();
		}
	}

	@Override
	public void onClick(View v) {
		//返回上一页
		if(v == ib_back){
			this.finish();
		}
		//返回首页
		if(v == tv_update){
			intent = new Intent(this, MainActivity.class);
			this.startActivity(intent);
			this.finish();
		}
		//留言
		if(v == btn_ok){
			//登陆异常    获取不到用户信息
			if(userName != null){
				//留言内容不能为空<--注意前面的【!】
				if(!et_content.getText().toString().equals("") && et_content.getText().toString() != null){
					HashMap<String, Object> params = new HashMap<String, Object>();
					content = et_content.getText().toString(); 
					params.put("content", content);
					params.put("username", userName);
					params.put("receivename", receiveName);
					HttpHelper.asyncPost(Api.URL + "/second-hand/msg_add.do", params, new Callback() {
						
						@Override
						public void dataLoaded(Message msg) {
							if(HttpStatus.SC_OK != msg.what){ //网络请求成功
								AppException.http(msg.what).makeToast(LeaveMessageActivity.this);
								return;
							} 
							Toast.makeText(LeaveMessageActivity.this, "留言成功", Toast.LENGTH_SHORT).show();
							LeaveMessageActivity.this.finish();
						}
					});
				} else {
					Toast.makeText(LeaveMessageActivity.this, "内容不能为空，请您您填写内容!", Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(LeaveMessageActivity.this, "登陆异常！请返回后重新留言", Toast.LENGTH_SHORT).show();
			}
		}
		//对话框的取消按钮
		if(v == btn_back){
			//关闭对话框
			dialog.dismiss();
			LeaveMessageActivity.this.finish();
		}
		//对话框的取消按钮
		if(v == btn_determine){
			//确定后跳到登录页面          
			dialog.dismiss();
			Intent intent = new Intent(this,LoginActivity.class);
			this.startActivity(intent);
		}
		
	}

	
}
