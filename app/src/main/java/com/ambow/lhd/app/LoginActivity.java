package com.ambow.lhd.app;
/**
 * 登陆、注册
 */
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ambow.lhd.api.Api;
import com.ambow.lhd.common.AppException;
import com.ambow.lhd.common.HttpHelper;
import com.ambow.lhd.common.HttpHelper.Callback;
import com.ambow.lhd.entity.Users;
import com.ambow.lhd.service.MyApplication;

public class LoginActivity extends Activity implements OnClickListener {

	private Button btn_back;// 取消
	private Button btn_login;// 登陆
	// private Button btn_right;
	private EditText et_name;
	private EditText et_pwd;
	private TextView tv_register;// 注册
	private TextView tv_warn;// 警告提示
	private View RelativeLayout;

	private HashMap<String, Object> params;// 请求参数
	private Users user;// 用户实体
	private SharedPreferences sp;// 偏好设定
	private MyApplication myApplication;// 数据传递
	private Dialog dialog;// 状态对话框

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		// 存数据======
		myApplication = (MyApplication) this.getApplicationContext();
		myApplication.addActivity(this);
		// 使用SharedPreferences进行数据存储
		sp = this.getSharedPreferences("User.xml", MODE_PRIVATE);

		btn_back = (Button) this.findViewById(R.id.login_btn_back);
		btn_login = (Button) this.findViewById(R.id.login_btn_login);
		et_name = (EditText) this.findViewById(R.id.login_et_name);
		et_pwd = (EditText) this.findViewById(R.id.login_et_pwd);
		tv_warn = (TextView) this.findViewById(R.id.login_tv_warn);
		RelativeLayout = this.findViewById(R.id.login_rellay4);
		tv_register = (TextView) this.findViewById(R.id.login_tv_register);
		
		// 判断是否存在或者是空值
		if (sp.getString("user", "") != null || !sp.getString("user", "").equals("")) {
			et_name.setText(sp.getString("user", ""));
			et_pwd.setText(sp.getString("pwd", ""));
		}

		btn_back.setOnClickListener(this);
		btn_login.setOnClickListener(this);
		et_name.setOnClickListener(this);
		et_pwd.setOnClickListener(this);
		tv_warn.setOnClickListener(this);
		RelativeLayout.setOnClickListener(this);
		tv_register.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// 取消
		if (v == btn_back) {
			Intent intent = new Intent(LoginActivity.this, MainActivity.class);
			LoginActivity.this.startActivity(intent);
			LoginActivity.this.finish();
		}
		// 注册
		if (v == tv_register) {
			Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
			startActivity(intent);
		}
		// 登陆
		if (v == btn_login) {
			
			 if(checked()){
				if(HttpHelper.IsHaveInternet(LoginActivity.this)){
					//显示对话框界面
					View view = getLayoutInflater().inflate(R.layout.progress_dialog, null, false);
					//对话框样式
					dialog = new Dialog(LoginActivity.this,R.style.myDialogTheme);
					dialog.setContentView(view);
					dialog.show();
					
					params = new HashMap<String, Object>();
					params.put("username", et_name.getText().toString());
					params.put("pwd", et_pwd.getText().toString());
					HttpHelper.asyncPost(Api.URL+"/second-hand/login.do", params, new Callback() {
						//重写dataLoaded函数
						@Override
						public void dataLoaded(Message msg) {
							if(msg.what == 200){
								if(msg.obj.toString().equals("error1")){
									RelativeLayout.setVisibility(View.VISIBLE);
									dialog.dismiss();//隐藏对话框
									tv_warn.setText("用户名错误！");
								}
								else if(msg.obj.toString().equals("error2")){
									RelativeLayout.setVisibility(View.VISIBLE);
									dialog.dismiss();
									tv_warn.setText("密码错误！");
								}
								else {
									user = new Users();
								  try {
									JSONObject josn = new JSONObject(msg.obj.toString());
									user.setUserId(josn.getInt("userId"));
									user.setUserName(josn.getString("userName"));
									user.setPassword(josn.getString("password"));
									user.setEmail(josn.getString("email"));
									user.setSchool(josn.getString("school"));
									user.setCourt(josn.getString("court"));
									user.setProfessional(josn.getString("professional"));
									
									if(!sp.getString("user", "").equals(josn.getString("userName"))){
										Editor eidt = sp.edit();
										eidt.putString("user", josn.getString("userName"));
										eidt.putString("pwd", josn.getString("password"));
										eidt.commit();//直接写入磁盘,有返回值
									}
									dialog.dismiss();
									myApplication.userMap.put("user", user);
									Toast.makeText(LoginActivity.this, "  登陆成功！ ", 0).show();
									LoginActivity.this.fileList();
									LoginActivity.this.finish();
									
//									//跳到主页面
//									Intent intent = new Intent(LoginActivity.this,MainActivity.class);
//									startActivity(intent);
									
								  } catch (JSONException e) {
										e.printStackTrace();
								  }
								}
							}else {
								dialog.dismiss();
								AppException.http(msg.what).makeToast(LoginActivity.this);
							}
						}
					});
				}else {
					Toast.makeText(LoginActivity.this, "无网络连接",Toast.LENGTH_LONG).show();
				}
			}
		}
	}
	//判断用户名密码是否为空
	private boolean checked() {
		if(et_name.getText().toString() == null || et_name.getText().toString().equals("")){
			RelativeLayout.setVisibility(View.VISIBLE);
			tv_warn.setText("  用户名不能为空!  ");
			return false;
		}
		else if(et_pwd.getText().toString() == null || et_pwd.getText().toString().equals("")){
			RelativeLayout.setVisibility(View.VISIBLE);
			tv_warn.setText("  密码不能为空!  ");
			return false;
		}
		else {
			return true;
		}
	}
}