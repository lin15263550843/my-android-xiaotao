package com.ambow.lhd.app;
/**
 * 登陆后显示的主页面
 */
import com.ambow.lhd.fragment.HomepageFragment;
import com.ambow.lhd.fragment.MineFragment;
import com.ambow.lhd.fragment.TypeFragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class MainActivity extends FragmentActivity  implements OnClickListener{
	
	private ImageButton ibtn_homepage;
	private ImageButton ibtn_type;
	private ImageButton ibtn_mine;
	
	private HomepageFragment homepageFragment;
	private TypeFragment typeFragment;
	private MineFragment mineFragment;
	//Fragment管理器
	private FragmentManager fragmentManager;
	//当前的fragment页面
	private Fragment content;
	
	private static String[] FRAGMENT_TAG = {"homepage","type","mine"};
	
	//存放图片
	private int[] allButtons = {R.id.btn_main_bottom_homepage,R.id.btn_main_bottom_type,R.id.btn_main_bottom_mine};
	//图片
	private int[] allTrueSelImg = {R.drawable.homepage_true,R.drawable.type_true,R.drawable.mine_true};
	private int[] allFaseSelImg = {R.drawable.homepage_fase,R.drawable.type_fase,R.drawable.mine_fase};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		initFragment();
		initView();
//		AppManager.getAppManager().addActivity(this);
	}

	private void initView() {
		ibtn_homepage = (ImageButton) this.findViewById(R.id.btn_main_bottom_homepage);
		ibtn_type = (ImageButton) this.findViewById(R.id.btn_main_bottom_type);
		ibtn_mine = (ImageButton) this.findViewById(R.id.btn_main_bottom_mine);
		
		ibtn_homepage.setOnClickListener(this);
		ibtn_type.setOnClickListener(this);
		ibtn_mine.setOnClickListener(this);
		//拿到管理器
		fragmentManager = getSupportFragmentManager();
		//首次登陆成功后显示首页
		showView(ibtn_homepage);
	}
	
	private void initFragment() {
		//创建fragment对象
		homepageFragment = new HomepageFragment();
		typeFragment = new TypeFragment();
		mineFragment = new MineFragment();
	}

	@Override
	public void onClick(View v) {
		if(v == ibtn_homepage){
			showView(ibtn_homepage);
		}
		if(v == ibtn_type){
			showView(ibtn_type);
		}
		if(v == ibtn_mine){
			showView(ibtn_mine);
		}
	}

	private void showView(View v) {
		//全部先变成灰色
    	for(int i=0;i<allButtons.length;i++){
    		ImageButton ibtn = (ImageButton)findViewById(allButtons[i]);
    		ibtn.setImageDrawable(getResources().getDrawable(allFaseSelImg[i]));
    	}
    	for(int i=0; i<allButtons.length;i++){
    		if(allButtons[i] == v.getId()){ //表示当前点击按钮
    			ImageButton ibtn = (ImageButton)findViewById(allButtons[i]);
    			ibtn.setImageDrawable(getResources().getDrawable(allTrueSelImg[i]));
    			break;
    		}
    	}
    	if(v == ibtn_homepage){
    		switchContent(homepageFragment, 0);
    	}
    	if(v == ibtn_type){
    		switchContent(typeFragment, 1);
    	}
    	if(v == ibtn_mine){
    		switchContent(mineFragment, 2);
    	}
//    	fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
	}
	//切换
	public void switchContent(Fragment f,int index){
		if(content != f){ //若果点击的是当前页面不切换
			FragmentTransaction transaction = fragmentManager.beginTransaction();
			if(!f.isAdded()){//判断是否加过
				if(content == null){//当前页面不为空
					//用fragment替换当前页面	
					transaction.add(R.id.main_content, f,FRAGMENT_TAG[index]).commit();
				}else{
					transaction.hide(content).add(R.id.main_content, f).commit();
				}
			}else{
				transaction.hide(content).show(f).commit();
			}
			content = f;
		}
	}
}
