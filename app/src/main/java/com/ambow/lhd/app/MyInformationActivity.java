package com.ambow.lhd.app;

import android.app.Activity;
import android.os.Bundle;

/**
 * 个人信息
 * 
 * @author 林东
 * 
 */
public class MyInformationActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_information);
	}

}
