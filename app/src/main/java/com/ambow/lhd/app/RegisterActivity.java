package com.ambow.lhd.app;
/**
 * 注册
 */
import java.util.HashMap;

import com.ambow.lhd.api.Api;
import com.ambow.lhd.common.HttpHelper;
import com.ambow.lhd.common.HttpHelper.Callback;
import com.ambow.lhd.service.MyApplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends Activity implements OnClickListener{
	
	private MyApplication myApplication;
	private HashMap<String, Object> params;
	
	private ImageButton register_ib_banck;//返回
	private Button register_btn_register;//注册
	private EditText register_et_user; 
	private EditText register_et_pwd;
	private EditText register_et_email;
	private EditText register_et_school;
	private EditText register_et_floor;
	private EditText register_et_phone;
	private CheckBox register_cb_agreement;
	private TextView register_tv_warn;
	
	
	private String username;
	private String pwd;
	private String email;
	private String school;
	private String court;
	private String professional;
	private boolean agreement;
	
	private String phone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		myApplication = (MyApplication) this.getApplicationContext();
		myApplication.addActivity(this);
		
		register_ib_banck = (ImageButton)this.findViewById(R.id.register_ib_banck);
		register_btn_register = (Button)this.findViewById(R.id.register_btn_register);
		register_et_user = (EditText)this.findViewById(R.id.register_et_username);
		register_et_pwd = (EditText)this.findViewById(R.id.register_et_pwd);
		register_et_email = (EditText)this.findViewById(R.id.register_et_email);
		register_et_school = (EditText)this.findViewById(R.id.register_et_school);
		register_et_floor = (EditText)this.findViewById(R.id.register_et_floor);
		register_et_phone = (EditText)this.findViewById(R.id.register_et_phone);
		register_cb_agreement = (CheckBox) this.findViewById(R.id.register_cb_agreement);
		register_tv_warn = (TextView) this.findViewById(R.id.register_tv_warn);
		
		register_ib_banck.setOnClickListener(this);
		register_btn_register.setOnClickListener(this);
		register_et_user.setOnClickListener(this);
		register_et_pwd.setOnClickListener(this);
		register_et_email.setOnClickListener(this);
		register_et_school.setOnClickListener(this);
		register_et_floor.setOnClickListener(this);
		register_et_phone.setOnClickListener(this);
		register_cb_agreement.setOnClickListener(this);
		register_tv_warn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		//返回
		if(v == register_ib_banck){
			Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
			RegisterActivity.this.startActivity(intent);
			RegisterActivity.this.finish();
		}
		//注册
		if(v == register_btn_register){
			if(checked()){
				params = new HashMap<String, Object>();
				params.put("username", username);
				params.put("pwd", pwd);
				params.put("email", email);
				params.put("school", school);
				params.put("court", court);
				params.put("professional", professional);
				
				HttpHelper.asyncPost(Api.URL + "/second-hand/add_user.do", params, new Callback() {
					
					@Override
					public void dataLoaded(Message msg) {
						if(msg.what == 200){
							if(msg.obj.toString().equals("用户名已被注册！")){
//								register_tv_warn.setText("用户名已被注册！");
								Toast.makeText(RegisterActivity.this, "用户名已被注册！", Toast.LENGTH_LONG).show();
							}else {
								Toast.makeText(RegisterActivity.this, msg.obj.toString(), Toast.LENGTH_LONG).show();
								RegisterActivity.this.finish();
							}
						}else {
							Toast.makeText(RegisterActivity.this, "注册失败，连接超时了！", Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		}
	}
	
	
	//判断所填的信息是否为空
	private boolean checked(){
		username = register_et_user.getText().toString();
		pwd = register_et_pwd.getText().toString();
		email = register_et_email.getText().toString();
		school = register_et_school.getText().toString();
		court = register_et_floor.getText().toString();
		professional = register_et_phone.getText().toString();
		agreement = register_cb_agreement.isChecked();
		
		if(username == null || username.equals("")){
			Toast.makeText(this, "用户名不能为空！", 1).show();
			return false;
		}
		if(pwd == null || pwd.equals("")){
			Toast.makeText(this, "密码不能为空！", 1).show();
			return false;
		}
		if(email == null || email.equals("")){
			Toast.makeText(this, "邮箱不能为空！", 1).show();
			return false;
		}
		if(school == null || school.equals("")){
			Toast.makeText(this, "学校不能为空！", 1).show();
			return false;
		}
		if(court == null || court.equals("")){
			Toast.makeText(this, "宿舍楼不能为空！", 1).show();
			return false;
		}
		if(!agreement){
			Toast.makeText(this, "必须同意用户协议才能注册！", 1).show();
			return false;
		}
		return true;
	}
	
}
