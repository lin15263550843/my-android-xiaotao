package com.ambow.lhd.app;


import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpStatus;

import com.ambow.lhd.api.Api;
import com.ambow.lhd.common.AppException;
import com.ambow.lhd.common.HttpHelper;
import com.ambow.lhd.common.HttpHelper.Callback;
import com.ambow.lhd.entity.Users;
import com.ambow.lhd.service.MyApplication;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

/**
 * 发布物品
 * 
 * @author 林东
 * 
 */
public class ReleaseActivity extends Activity implements OnClickListener, OnItemSelectedListener {
	/*
	 * private View view, dialogView; private 
	 * TextView title, lable, gallery,
	 * camera;// Spinner的选项，对话框的相册和相机 private ImageView addImageView; private
	 * LinearLayout addPicLayout; private Bitmap bitmap; private
	 * ArrayList<Bitmap> bitmap_list = new ArrayList<Bitmap>(); private
	 * ArrayList<String> photo_list = new ArrayList<String>();// 图片的路径集合 private
	 * String picturePath = null;// 图片的存放路径 // private Shop shop;// 发布信息实体
	 * private String type = "fffff";
	 */
	private MyApplication myApplication;
	private Users users;
	private Intent intent;
	private View view;
	private String type = "fffff"; 
	private Spinner spinner_type; //下拉菜单
	private ImageView spinner_iv_icon; //类型下拉列表选中图片显示
	private TextView spinner_tv_label; //类型下拉列表内容
	private String[] titles = { "自行车", "电子产品", "书刊杂志", "运动健身", "生活用品", "办公用品" };
	private ArrayList<String> photo_list = new ArrayList<String>();// 图片的路径集合
	
	private ImageButton btn_back; // 返回主页
	private TextView tv_type; // 发布
	private TextView tv_update; // 我的发布

	private EditText et_sname; // 发布物品标题名称
	private EditText et_price; // 物品价格
	private EditText et_phones; // 电话号码
	private EditText et_description; // 物品描述内容
	private Button btn_register; // 发布

	private View dialogView; // 确认取消发布对话框
	private Dialog dialog; // 弹出对话框
	private Button dialog_btn_back; // 取消取消发布
	private Button dialog_btn_determine; // 取消发布
	private TextView dialog_tv_content; // 提示内容
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.release);

		myApplication = (MyApplication) this.getApplicationContext();
		myApplication.addActivity(this);
		users = (Users) myApplication.userMap.get("user");

		btn_back = (ImageButton) findViewById(R.id.type_title_ib_back);
		tv_type = (TextView) findViewById(R.id.type_title_tv_type);
		tv_update = (TextView) findViewById(R.id.type_title_tv_update);

		et_sname = (EditText) findViewById(R.id.release_et_sname);
		et_price = (EditText) findViewById(R.id.release_et_price);
		et_phones = (EditText) findViewById(R.id.release_et_phones);
		et_description = (EditText) findViewById(R.id.release_et_description);
		btn_register = (Button) findViewById(R.id.register_btn_register);
		
		spinner_type = (Spinner) this.findViewById(R.id.release_spinner_type);//下拉菜单

		// 确认取消发布对话框
		dialogView = getLayoutInflater().inflate(R.layout.alert_dialog, null, false);
		dialog_tv_content = (TextView) dialogView.findViewById(R.id.alert_dialog_tv_content);
		// 创建对话框
		dialog = new Dialog(this, R.style.myDialogTheme);
		// 对话框事件
		// 注意 必须通过<dialogView.findViewById()>查找布局文件中的指定Id的组件,findViewById()找不到组件
		dialog_btn_back = (Button) dialogView.findViewById(R.id.alert_dialog_btn_back);
		dialog_btn_determine = (Button) dialogView.findViewById(R.id.alert_dialog_btn_determine);

		// 注册监听事件
		btn_back.setOnClickListener(this);
		tv_type.setText("发布");
		tv_update.setText("我的发布");
		tv_update.setCompoundDrawables(null, null, null, null);
		tv_update.setOnClickListener(this);
		btn_register.setOnClickListener(this);
		
		// 对话框监听事件
		dialog_tv_content.setText("您确定要取消发布吗？你所填写的内容将丢失！");
		dialog_btn_back.setOnClickListener(this);
		dialog_btn_determine.setOnClickListener(this);
		
		//二手类型适配器
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, titles) {

			@Override
			public View getDropDownView(int position, View convertView,ViewGroup parent) {
				
				//下拉菜单列表
				view = getLayoutInflater().inflate(R.layout.spinner_item, parent, false);
				spinner_tv_label = (TextView) view.findViewById(R.id.spinner_tv_label);
				spinner_iv_icon =  (ImageView) view.findViewById(R.id.spinner_iv_icon);
				//下拉菜单显示各种类型名称getItem(position)
				spinner_tv_label.setText(getItem(position));
				//选择        
				if(spinner_type.getSelectedItemPosition() == position){
					spinner_tv_label.setTextColor(getResources().getColor(R.color.selected_bg));
					view.setBackgroundColor(getResources().getColor(R.color.selected_bg));
					spinner_iv_icon.setVisibility(View.VISIBLE);
				}
				return view;
			}
		};
		spinner_type.setAdapter(adapter);
		spinner_type.setOnItemSelectedListener(this);
	}

	@Override
	public void onClick(View v) {
		// 返回主页
		if (v == btn_back) {
			// 弹出
			dialog.setContentView(dialogView);
			dialog.show();
		}
		// 我的发布
		if (v == tv_update) {
			intent = new Intent(this, MyReleaseListActivity.class);
			this.startActivity(intent);
			this.finish();
		}
		// 发布信息
		if (v == btn_register) {
				
			if (checked()) {
				//???????
//				dialog.setContentView(view);
//				dialog.show();
				
				String picture = "";
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("shopname", et_sname.getText().toString());
				params.put("price", et_price.getText().toString());
				params.put("userName", users.getUserName());
				params.put("userPhone", et_phones.getText().toString());
				params.put("description", et_description.getText().toString());
				params.put("category", type);
				params.put("picture", picture);
				Log.v("-----params-----", params.toString());
				HashMap<String, File> fileMap = new HashMap<String, File>();
				for (int i = 0; i < photo_list.size(); i++) {
					fileMap.put("fileMap" + i, new File(photo_list.get(i)));
				}
				Log.v("-------fileMap-------", fileMap.toString());
				HttpHelper.asyncMultipartPost(Api.URL + "/second-hand/shop_add.do", params, fileMap, new  Callback() {
					
					@Override
					public void dataLoaded(Message msg) {
						if (HttpStatus.SC_OK != msg.what) {
							dialog.dismiss();
							AppException.http(msg.what).makeToast(
									ReleaseActivity.this);
							return;
						}
						dialog.dismiss();
						String message = (String) msg.obj;
						Toast.makeText(ReleaseActivity.this,
								message, Toast.LENGTH_LONG).show();
						ReleaseActivity.this.finish();
					}
				});
			}
		}
		// 确认取消发布
		if (v == dialog_btn_determine) {
			dialog.dismiss();
			intent = new Intent(this, MainActivity.class);
			this.startActivity(intent);
			this.finish();
		}
		// 取消取消发布
		if (v == dialog_btn_back) {
			// 关闭对话框
			dialog.dismiss();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		type = titles[position];
		Log.v("-----------type-----------", type);
		}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
	}
	
	//验证输入信息是否正确
	private boolean checked() {
		if (et_sname.getText().toString() == null || et_sname.getText().toString().equals("")) {
			Toast.makeText(this, "标题不能为空", Toast.LENGTH_SHORT).show();
			return false;
		} else if (et_price.getText().toString() == null || et_price.getText().toString().equals("")) {
			Toast.makeText(this, "价格不能为空", Toast.LENGTH_SHORT).show();
			return false;
		} else if (et_phones.getText().toString() == null || et_phones.getText().toString().equals("") || et_phones.length() != 11) {
			Toast.makeText(this, "电话是十一位", Toast.LENGTH_SHORT).show();
			return false;
		} else {
			return true;
		}
	}
}
