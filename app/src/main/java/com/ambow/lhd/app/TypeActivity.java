package com.ambow.lhd.app;
/**
 * 测试用的，可以删掉
 */
import java.util.ArrayList;
import java.util.HashMap;

import com.ambow.lhd.app.R;
import com.ambow.lhd.app.ShopListActivity;
import com.ambow.lhd.service.MyApplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class TypeActivity extends BaseActivity implements OnItemClickListener {

	// 需要显示的类型图片
	private int[] images = { R.drawable.type_1, R.drawable.type_2, R.drawable.type_3, R.drawable.type_4, R.drawable.type_5, R.drawable.type_6};
	// 需要显示的类型标题
	private String[] titles = { "自行车", "电子产品", "书刊杂志", "运动健身", "生活用品", "办公用品" };
	private MyApplication MyApplication;
	private ImageButton ib_back;
	private TextView tv_update;

	private GridView gv_type;
	private Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.type);
		
		MyApplication = (MyApplication) this.getApplicationContext();
		MyApplication.addActivity(this);
		ib_back = (ImageButton) findViewById(R.id.type_title_ib_back);
		tv_update = (TextView) findViewById(R.id.type_title_tv_update);
		gv_type = (GridView) findViewById(R.id.type_gv_type);
		
		ib_back.setVisibility(View.GONE);
		tv_update.setVisibility(View.GONE);
		gv_type.setAdapter(getMenuAdapter(titles, images));
		gv_type.setOnItemClickListener(this);
		
	}
	/**
	 * 1）AdapterView<?> parent,  
	 * parent相当于listview 适配器的一个指针，可以通过它来获得listview里装着的一切东西
	 * （2）View view,     
	 * view是你点b item的view的句柄，就是你可以用这个view，来获得b里的控件的id后操作控件   <--- 这里需特别注意
	 * （3） int position,   
	 * position是b在适配器里id     的位置（生成listview时，适配器一个一个的做item，然后把他们按顺序排好队，在放到listview里，意思就是这个b是第position号做好的）
	 * （4） long id
	 * 是b在listview 里的第几行的位置（很明显是第2行），在没有headerView，用户添加的view以及footerView的情况下position和id的值是一样的。
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		System.out.println("---???---111");
		switch (position) {
		case 0:
			System.out.println("---???---222");
			intent = new Intent(TypeActivity.this, ShopListActivity.class);
			intent.putExtra("type", "自行车");
			startActivity(intent);
			break;
			
		case 1:
			intent = new Intent(TypeActivity.this, ShopListActivity.class);
			intent.putExtra("type", "电子产品");
			startActivity(intent);
			break;
			
		case 2:
			intent = new Intent(TypeActivity.this, ShopListActivity.class);
			intent.putExtra("type", "书刊杂志");
			startActivity(intent);
			break;
			
		case 3:
			intent = new Intent(TypeActivity.this, ShopListActivity.class);
			intent.putExtra("type", "运动健身");
			startActivity(intent);
			break;
			
		case 4:
			intent = new Intent(TypeActivity.this, ShopListActivity.class);
			intent.putExtra("type", "生活用品");
			startActivity(intent);
			break;
			
		case 5:
			intent = new Intent(TypeActivity.this, ShopListActivity.class);
			intent.putExtra("type", "办公用品");
			startActivity(intent);
			break;

		default:
			break;
		} 
	}
	
	private SimpleAdapter getMenuAdapter(String[] titles, int[] images){
		
		ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String,Object>>();
		for (int i = 0; i < titles.length; i++) {
			HashMap<String, Object> hm = new HashMap<String, Object>();
			hm.put("itemImage", images[i]);
			hm.put("itemTitle", titles[i]);
			data.add(hm);
		}
		
		SimpleAdapter simpleAdapter = new SimpleAdapter(this, data, R.layout.type_gridview, 
				new String[] { "itemImage", "itemTitle"}, new int[] {R.id.type_girdview_iv_1, R.id.type_girdview_tv_1});
		return simpleAdapter;
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);
	}
}
