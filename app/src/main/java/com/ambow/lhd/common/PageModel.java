package com.ambow.lhd.common;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ambow.lhd.entity.ItemList;
import com.ambow.lhd.entity.Messages;

/**
 * 数据转化模型
 * 
 * @author 林东
 * @param <T>
 */
public class PageModel<T> implements Serializable {
	private static final long serialVersionUID = -5259112447890337702L;
	private int pageNo = 1;
	private int pageSize = 10;
	private long recordCount;
	private int pageCount;
	private List<T> data;

	public PageModel() {
	}

	public PageModel(int pageSize, int pageNo) {
		super();
		this.pageSize = pageSize;
		this.pageNo = pageNo;
	}

	// 把json数据转换成pm模型-----发布信息
	public static PageModel<ItemList> jsonConvert(String json) {
		ItemList itemList = null;
		List<ItemList> list = new ArrayList<ItemList>();
		PageModel<ItemList> pm = new PageModel<ItemList>();
		try {
			JSONObject jsonObject = new JSONObject(json);
			pm.setPageNo(jsonObject.getInt("pageNo"));
			pm.setPageSize(jsonObject.getInt("pageSize"));
			pm.setRecordCount(jsonObject.getLong("recordCount"));
			JSONArray array = jsonObject.getJSONArray("data");

			for (int i = 0; i < array.length(); i++) {
				JSONObject obj = array.getJSONObject(i);
				itemList = new ItemList();
				//注意要用 opt 不能用 get 
				//因为get()取值不正确会抛出异常，必须用try catch或者throw包起
				//而opt()取值不正确则会试图进行转化或者输出友好值，不会抛出异常
				itemList.setShopId(obj.optInt("shopId")); // 商品id
				itemList.setCategory(obj.optString("category")); // 商品类别
				itemList.setDescription(obj.getString("description")); // 商品描述
				itemList.setPicture(obj.optString("picture")); // 商品图片
				itemList.setPrice(obj.optString("price"));// 商品价格
				itemList.setSchool(obj.optString("school")); // 用户所在学校
				itemList.setCourt(obj.optString("court")); // 用户所在宿舍楼
				itemList.setShopName(obj.optString("shopname"));// 商品名
				itemList.setUserName(obj.optString("userName")); // 发布者姓名
				itemList.setUserPhone(obj.optString("userPhone")); // 用户电话
				long time = obj.optLong("put_time"); // 发布时间
				Date date = new Date(time);
				itemList.setPutTime(date);
				list.add(itemList);
			}
			pm.setData(list);
			System.out.println("-----PageModel---pm.setData(list)----"
					+ pm.getData());

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return pm;
	}

	// json数据格式转换成pm模型 ----- 留言
	public static PageModel<Messages> jsonConvertMsg(String json) {
		Messages message = null;
		List<Messages> list = new ArrayList<Messages>();
		PageModel<Messages> pm = new PageModel<Messages>();
		try {
			JSONObject jsonObject = new JSONObject(json);
			pm.setPageNo(jsonObject.getInt("pageNo"));
			pm.setPageSize(jsonObject.getInt("pageSize"));
			pm.setRecordCount(jsonObject.getLong("recordCount"));
			JSONArray array = jsonObject.getJSONArray("data");
			for (int i = 0; i < array.length(); i++) {

				JSONObject obj = array.getJSONObject(i);
				message = new Messages();
				message.setContent(obj.getString("content"));
				Long time = obj.optLong("leave_time");
				Date date = new Date(time);
				message.setLeave_time(date);
				message.setUsername(obj.getString("username"));
				message.setReceivename(obj.getString("receivename"));
				list.add(message);
			}

			pm.setData(list);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return pm;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setRecordCount(long recordCount) {
		this.recordCount = recordCount;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	public int getPageCount() {
		pageCount = (int) ((recordCount + pageSize - 1) / pageSize);
		return pageCount;
	}

	@Override
	public String toString() {
		return "PageModel [pageNo=" + pageNo + ", pageSize=" + pageSize
				+ ", recordCount=" + recordCount + ", pageCount=" + pageCount
				+ ", data=" + data + "]";
	}

}
