package com.ambow.lhd.entity;

import java.io.Serializable;
import java.util.Date;

public class ItemList implements Serializable {
	
	private static final long serialVersionUID = 6830885499876826670L;
	private int shopId; //商品ID
	private String shopName; //商品名
	private String description; //商品描述
	private String userPhone; //用户电话
	private String category; //商品类型
	private String picture; //商品图片
	private String price; //商品价格
	private Date  putTime; //发布时间
	private String userName; //用户名
	private String school; //用户所在学校
	private String court; //用户所在宿舍楼
	public int getShopId() {
		return shopId;
	}
	public void setShopId(int shopId) {
		this.shopId = shopId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserPhone() {
		return userPhone;
	}
	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public Date getPutTime() {
		return putTime;
	}
	public void setPutTime(Date putTime) {
		this.putTime = putTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public String getCourt() {
		return court;
	}
	public void setCourt(String court) {
		this.court = court;
	}
	
	public String getPicture() {
		return picture;
	}
	public void setPicture(String picture) {
		this.picture = picture;
	}
	@Override
	public String toString() {
		return "ItemList [shopId=" + shopId + ", shopName=" + shopName
				+ ", description=" + description + ", userPhone=" + userPhone
				+ ", category=" + category + ", price=" + price + ", putTime="
				+ putTime + ", userName=" + userName + ", school=" + school
				+ ", court=" + court + "]";
	}

}
