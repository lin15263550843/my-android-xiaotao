package com.ambow.lhd.entity;
/**
 * 用户实体类
 * @author 林东
 *
 */
public class Users {
	private int userId;      	   // '用户id',
	private String userName;       //  '用户名',
	private String password;       //  '用户密码',
	private String email;          //  '用户邮箱',
	private String school;         //  '用户所在学校',
	private String court;          //  '用户所在宿舍楼',
	private String professional;   //  '用户预留电话'
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSchool() {
		return school;
	}
	public void setSchool(String school) {
		this.school = school;
	}
	public String getCourt() {
		return court;
	}
	public void setCourt(String court) {
		this.court = court;
	}
	public String getProfessional() {
		return professional;
	}
	public void setProfessional(String professional) {
		this.professional = professional;
	}
	@Override
	public String toString() {
		return "Users [userId=" + userId + ", userName=" + userName
				+ ", password=" + password + ", email=" + email + ", school="
				+ school + ", court=" + court + ", professional="
				+ professional + "]";
	}
	
	
}
