package com.ambow.lhd.fragment;
/**
 * 首页
 */
import com.ambow.lhd.app.DemandActivity;
import com.ambow.lhd.app.LoginActivity;
import com.ambow.lhd.app.MyCollectActivity;
import com.ambow.lhd.app.R;
import com.ambow.lhd.app.ReleaseActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

public class HomepageFragment extends Fragment implements OnClickListener{
	
	private View contentView; 
	Intent intent;
	private ImageButton ib_back; //返回登陆
	private TextView tv_release; //发布
	private TextView tv_collect; //收藏
	private TextView tv_demand; //求购

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		contentView = inflater.inflate(R.layout.homepage,container,false);
		
		initView();
		
		return contentView;
	}

	private void initView() {
		
		ib_back = (ImageButton) contentView.findViewById(R.id.homepage_ib_back);
		tv_release = (TextView) contentView.findViewById(R.id.homepage_tv_release);
		tv_collect = (TextView) contentView.findViewById(R.id.homepage_tv_collect);
		tv_demand = (TextView) contentView.findViewById(R.id.homepage_tv_demand);
		
		//注册监听事件
		ib_back.setOnClickListener(this);
		tv_release.setOnClickListener(this);
		tv_collect.setOnClickListener(this);
		tv_demand.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		//返回登陆
		if(v == ib_back){
			intent = new Intent(getActivity(), LoginActivity.class);
			startActivity(intent);
		}
		//发布
		if(v == tv_release){
			intent = new Intent(getActivity(), ReleaseActivity.class);
			startActivity(intent);
		}
		//收藏
		if(v == tv_collect){
			intent = new Intent(getActivity(), MyCollectActivity.class);
			startActivity(intent);
		}
		//求购
		if(v == tv_demand){
			intent = new Intent(getActivity(), DemandActivity.class);
			startActivity(intent);
		}
	}
}
