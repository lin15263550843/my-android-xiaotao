package com.ambow.lhd.fragment;

import android.app.Dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.ambow.lhd.app.LoginActivity;
import com.ambow.lhd.app.MyCollectActivity;
import com.ambow.lhd.app.MyDemandListActivity;
import com.ambow.lhd.app.MyInformationActivity;
import com.ambow.lhd.app.MyMessageListActivity;
import com.ambow.lhd.app.MyReleaseListActivity;
import com.ambow.lhd.app.R;
import com.ambow.lhd.app.Text;
import com.ambow.lhd.entity.Users;
import com.ambow.lhd.service.MyApplication;

/**
 * 个人中心
 * 
 * @author 林东
 *
 */
public class MineFragment extends Fragment implements OnClickListener {

	private MyApplication myApplication;
	private Users users;
	private String string;
	private View contentView;
	private Intent intent;
	
	private ImageButton ib_back; // 返回
	private TextView tv_type; // 个人中心
	private TextView tv_login; //登陆
	private TextView 	tv_name; //用户姓名
	
	private View rellay_my_information; // 个人信息
	private View rellay_my_demand; // 我的求购
	private View rellay_my_release; // 我的发布
	private View rellay_my_message; // 我的留言
	private View rellay_my_collect; // 我的收藏
	private View rellay_my_off; // 退出登录
	
	private View dialogView; //注销对话框的视图
	private Dialog dialog; //创建对话框
	private TextView dialog_tv_content; //对话框的内容
	private Button btn_back; //取消按钮
	private Button btn_determine; //确定注销按钮 
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		contentView = inflater.inflate(R.layout.mine, container, false);
		initView();
		return contentView;
	}
	
	private void initView() {
		
		myApplication = (MyApplication) getActivity().getApplicationContext();
		myApplication.addActivity(getActivity());
		users = (Users) myApplication.userMap.get("user");
		if (users != null) {  
			//如果用户名不为空则获取用户名
			string = users.getUserName();
		}
		ib_back = (ImageButton) contentView.findViewById(R.id.type_title_ib_back);
		tv_type = (TextView) contentView.findViewById(R.id.type_title_tv_type);
		tv_login = (TextView) contentView.findViewById(R.id.type_title_tv_update);
		tv_name = (TextView) contentView.findViewById(R.id.mine_tv_name);
		
		rellay_my_information = contentView.findViewById(R.id.mine_rellay_my_information);
		rellay_my_demand = contentView.findViewById(R.id.mine_rellay_my_demand);
		rellay_my_release = contentView.findViewById(R.id.mine_rellay_my_release);
		rellay_my_message = contentView.findViewById(R.id.mine_rellay_my_message);
		rellay_my_collect = contentView.findViewById(R.id.mine_rellay_my_collect);
		rellay_my_off = contentView.findViewById(R.id.mine_rellay_my_off);
		
		//用户注销确认对话框
		dialogView = getActivity().getLayoutInflater().inflate(R.layout.alert_dialog, null, false);
		//创建对话框
		dialog = new Dialog(getActivity(), R.style.myDialogTheme);
		//对话框事件    
		//注意  必须通过<dialogView.findViewById()>查找布局文件中的指定Id的组件,findViewById()找不到组件
		dialog_tv_content = (TextView) dialogView.findViewById(R.id.alert_dialog_tv_content);
		btn_back = (Button) dialogView.findViewById(R.id.alert_dialog_btn_back);
		btn_determine = (Button) dialogView.findViewById(R.id.alert_dialog_btn_determine);
	
		//注册监听事件
		ib_back.setOnClickListener(this);
		tv_type.setText("个人中心");
		tv_login.setOnClickListener(this);
		tv_login.setText("登陆");
		tv_name.setText(string);
		
		rellay_my_information.setOnClickListener(this);
		rellay_my_demand.setOnClickListener(this);
		rellay_my_release.setOnClickListener(this);
		rellay_my_message.setOnClickListener(this);
		rellay_my_collect.setOnClickListener(this);
		rellay_my_off.setOnClickListener(this);
		//注销对话框的
		dialog_tv_content.setText("您确定要退出当前用户吗？");
		btn_back.setOnClickListener(this); 
		btn_determine.setOnClickListener(this);
	}

	//点击事件处理
	@Override
	public void onClick(View v) {
		//返回
		if(v == ib_back){
			getActivity().finish();
		}
		//去登陆
		if(v == tv_login){
			intent = new Intent(getActivity(), LoginActivity.class);
			startActivity(intent);
		}
		//个人信息
		if(v == rellay_my_information){
			intent = new Intent(getActivity(), MyInformationActivity.class);
			startActivity(intent);
		}
		//我的求购
		if(v == rellay_my_demand){
			intent = new Intent(getActivity(), MyDemandListActivity.class);
			startActivity(intent);
		}
		//我的发布
		if(v == rellay_my_release){
			intent = new Intent(getActivity(), MyReleaseListActivity.class);
			startActivity(intent);
		}
		//我的留言
		if(v == rellay_my_message){
			intent = new Intent(getActivity(), MyMessageListActivity.class);
			startActivity(intent);
		}
		//我的收藏
		if(v == rellay_my_collect){
			intent = new Intent(getActivity(), MyCollectActivity.class);
			startActivity(intent);
		}
		//注销
		if(v == rellay_my_off){
			if(!myApplication.userMap.isEmpty()){
				//弹出对话框
				dialog.setContentView(dialogView);
				dialog.show();
			}
		}
		//注销对话框的取消按钮
		if(v == btn_back){
			//关闭对话框
			dialog.dismiss();
		}
		//注销对话框的取消按钮
		if(v == btn_determine){
			//确定后跳到登录页面          
			dialog.dismiss();
			intent = new Intent(getActivity(),LoginActivity.class);
			getActivity().startActivity(intent);
			getActivity().finish();
		}
	}
}
