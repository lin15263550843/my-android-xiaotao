package com.ambow.lhd.fragment;
/**
 * 类型
 */
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.ambow.lhd.app.R;
import com.ambow.lhd.app.ShopListActivity;
import com.ambow.lhd.service.MyApplication;

public class TypeFragment extends Fragment implements OnItemClickListener{
	
	private View contentView;
	
	// 需要显示的类型图片
	private int[] images = { R.drawable.type_1, R.drawable.type_2, R.drawable.type_3, R.drawable.type_4, R.drawable.type_5, R.drawable.type_6};
	// 需要显示的类型标题
	private String[] titles = { "自行车", "电子产品", "书刊杂志", "运动健身", "生活用品", "办公用品" };
	private MyApplication MyApplication;
	private ImageButton ib_back; //返回
	private TextView tv_update; 

	private GridView gv_type;
	
	private Intent intent;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		contentView = inflater.inflate(R.layout.type, container,false);
		initView();
		return contentView;
		
	}

	private void initView() {
		MyApplication = (MyApplication) getActivity().getApplicationContext();
		MyApplication.addActivity(getActivity());
		ib_back = (ImageButton) contentView.findViewById(R.id.type_title_ib_back);
		tv_update = (TextView) contentView.findViewById(R.id.type_title_tv_update);
		gv_type = (GridView) contentView.findViewById(R.id.type_gv_type);
		
		ib_back.setVisibility(View.GONE);
		tv_update.setVisibility(View.GONE);
		gv_type.setAdapter(getMenuAdapter(titles, images));
		gv_type.setOnItemClickListener(this);
		
	}

	

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		//在类型中显示所有类型并且把对应的“类型”传过去
		switch (position) {
		case 0:
			intent = new Intent(getActivity(), ShopListActivity.class);
			intent.putExtra("type", "自行车");
			startActivity(intent);
			break;
			
		case 1:
			intent = new Intent(getActivity(), ShopListActivity.class);
			intent.putExtra("type", "电子产品");
			startActivity(intent);
			break;
			
		case 2:
			intent = new Intent(getActivity(), ShopListActivity.class);
			intent.putExtra("type", "书刊杂志");
			startActivity(intent);
			break;
			
		case 3:
			intent = new Intent(getActivity(), ShopListActivity.class);
			intent.putExtra("type", "运动健身");
			startActivity(intent);
			break;
			
		case 4:
			intent = new Intent(getActivity(), ShopListActivity.class);
			intent.putExtra("type", "生活用品");
			startActivity(intent);
			break;
			
		case 5:
			intent = new Intent(getActivity(), ShopListActivity.class);
			intent.putExtra("type", "办公用品");
			startActivity(intent);
			break;

		default:
			break;
		} 
	}
	/**
	 * SimpleAdapter的作用是ArrayList和 GridView的桥梁。这个ArrayList里边的每一项都是一个 Map<String,?>类型。
	 * 
	 * 参数context：上下文，比如this。关联SimpleAdapter运行的视图上下文    <---注意---在Fragment中this要改成getActivity
	 * 参数data：Map列表，列表要显示的数据，这部分需要自己实现，如例子中的getData()，类型要与上面的一致，每条项目要与from中指定条目一致
	 * 参数resource：ListView单项布局文件的Id,这个布局就是你自定义的布局了，你想显示什么样子的布局都在这个布局中。这个布局中必须包括了to中定义的控件id
	 * 参数 from：一个被添加到Map上关联每一个项目列名称的列表，数组里面是列名称
	 * 参数 to：是一个int数组，数组里面的id是自定义布局中各个控件的id，需要与上面的from对应
	 * @param titles
	 * @param images
	 * @return
	 */
	private SimpleAdapter getMenuAdapter(String[] titles, int[] images){
		
		ArrayList<HashMap<String, Object>> data = new ArrayList<HashMap<String,Object>>();
		for (int i = 0; i < titles.length; i++) {
			HashMap<String, Object> hm = new HashMap<String, Object>();
			hm.put("itemImage", images[i]);
			hm.put("itemTitle", titles[i]);
			data.add(hm);
		}
		
		SimpleAdapter simpleAdapter = new SimpleAdapter(getActivity(), data, R.layout.type_gridview, 
				new String[] { "itemImage", "itemTitle"}, new int[] {R.id.type_girdview_iv_1, R.id.type_girdview_tv_1});
		return simpleAdapter;
	}
}
