package com.ambow.lhd.util;
/**
 * 程序助手
 */
import java.util.Stack;

import android.app.Activity;
import android.content.Context;

/**
 * 管理应用APP的Activity
 * @author user
 *
 */
public class AppManager {
	private static Stack<Activity> activityStack;
	private static AppManager instance;
	private AppManager(){
		
	}
	public static AppManager getAppManager(){
		if(instance == null){
			instance = new AppManager();
		}
		return instance;
	}
	/**
	 * 添加Activity到栈
	 */
	public void addActivity(Activity activity){
		if(activityStack == null)
			activityStack = new Stack<Activity>();
		activityStack.add(activity);
	}
	/**
	 * 获取当前的Activity（堆栈中最后一个压入的对象）
	 * @return
	 */
	public Activity currentActivity(){
		Activity activity = activityStack.lastElement();
		return activity;
	}
	/**
	 * 结束当前Activity
	 */
	public void finishActivity(){
		Activity activity = activityStack.lastElement();
		if(activity != null){
			activity.finish();
			activity = null;
		}
	}
	/**
	 * 结束指定的Activity
	 * @param activity
	 */
	public void finishActivity(Activity activity){
		if(activity != null){
			activityStack.remove(activity);
			activity.finish();
			activity = null;
		}
	}
	public void finishAllActivity(){
		for(int i=0,size=activityStack.size();i<size;i++){
			if(null != activityStack.get(i)){
				activityStack.get(i).finish();
			}
		}
		activityStack.clear();
	}
	public void appExit(Context context){
		finishAllActivity();
		/*ActivityManager activityMgr = (ActivityManager)context.getSystemService(
				Context.ACTIVITY_SERVICE);
		activityMgr.restartPackage(context.getPackageName());*/
		System.exit(0);
	}
}

